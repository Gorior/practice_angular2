import {Component, OnInit} from '@angular/core';
import {UsersService} from "./users.service";

@Component({
  moduleId: module.id,
  selector: 'users',
  styleUrls: ['users.css'],
  templateUrl: 'users.html'
})

export class UsersComponent implements OnInit {
  users: any[];

  constructor(private usersService: UsersService){

  }

  ngOnInit(){
    this.usersService.fetchUsers().subscribe(users => {
      this.users = users;
    })
  }

}
