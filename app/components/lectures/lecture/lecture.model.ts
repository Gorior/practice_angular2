interface ILecture {
  id?: number;
  number: number;
  title: string;
}

class LectureModel implements ILecture {
  id: number;
  number: number;
  title: string;

  constructor(lecture?: ILecture){
    if (lecture){
      this.id = lecture.id || null;
      this.number = lecture.number;
      this.title = lecture.title;
    }
  }
}

export { ILecture, LectureModel };
