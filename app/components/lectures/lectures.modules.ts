import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {LectureComponent} from "./lecture/lecture.component";
import {LecturesComponent} from "./lectures";
import {LectureDetailsModule} from "./lecture-details/lecture-details.module";
import {CreateLectureComponent} from "./create-lecture/create-lecture";
import {RouterModule} from "@angular/router";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    LectureDetailsModule
  ],
  declarations: [
    LectureComponent,
    LecturesComponent,
    CreateLectureComponent
  ],
  exports: [
    LecturesComponent
  ]
})

export class LecturesModule { }
